# Novel Download From WuxiaWorld

### Version 1.0

Download novel from WuxiaWorld and change it to pdf format. This is the first version of the bash script. It will be upgraded over time as I am new to bash scripting. This is version 1 and whenever I get time I will be upgrading over time.

- OS Used

    - Arch Linux

- Software needed 

    - Wget
    - paps
    - html2text
    - ps2pdf

#### [wget](https://github.com/mirror/wget)

GNU Wget is a free utility for non-interactive download of files from
the Web.  It supports HTTP, HTTPS, and FTP protocols, as well as
retrieval through HTTP proxies.

#### [paps](https://github.com/dov/paps)

paps is a command line program for converting Unicode text encoded in UTF-8 to postscript and pdf by using pango.


#### [html2text](https://github.com/aaronsw/html2text)

html2text is a Python script that converts a page of HTML into clean, easy-to-read plain ASCII text. Better yet, that ASCII also happens to be valid Markdown (a text-to-HTML format).

#### [ps2pdf](http://git.ghostscript.com/ghostpdl.git/)

- Need GhostScript installation

ps2pdf is a script that comes bundled with GhostScript, a freeware PostScript interpreter.

### How to Use:

Run the Script Using the Command.

```bash
./script.sh
```

It will Ask for the name of the novel given in the url format.

```url
https://www.wuxiaworld.com/novel/7-killers
```

Given the Above Url Enter the name of the novel given at the end, Example:

```
7-killers
```

It will Save the Each Chapter in pdf format.

