#!/bin/bash
#A Recursive script to download novel from wuxiaworld and save it to the ofline mode and read it as a pdf.

echo Enter the novel name from the url bar after https://wuxiaworld.com/novel/{novel-name}

read varname

wget -r -m -l1 -np --adjust-extension -R ".jpg .js .css .jpeg" http://www.wuxiaworld.com/novel/$varname

for file in `find www.wuxiaworld.com/novel/$varname -name "*.html"`; do html2text "$file" > "$file.txt"; done

for i in `find www.wuxiaworld.com/novel/$varname -name "*.txt"`; do paps "$i" > "$i.ps"; done

for j in `find www.wuxiaworld.com/novel/$varname -name "*.ps"`; do ps2pdf "$j" > "$j.pdf"; done

for k in `find -name "*.pdf"`; do mv "$k" www.wuxiaworld.com/novel/$varname; done

mkdir $varname

find www.wuxiaworld.com -name '*.pdf' -exec mv {} $varname/ \;

rm -rf www.wuxiaworld.com

cd $varname

rm *.ps.pdf

for f in *.html.*; do 
	mv -- "$f" "${f%.html.txt.pdf}.pdf"
done
